function addTag(tag) {
    console.log("handling tag " + tag);
    if (tag.endsWith(" ") && tag.length > 2) {
        document.getElementById("tags_input").value = "";

        const tagsDisplay = document.getElementById("tags_display");

        const tagElement = document.createElement("p");
        const textNode = document.createTextNode(tag);
        tagElement.appendChild(textNode);

        tagsDisplay.appendChild(tagElement);

        const tagsList = document.getElementById("tags");
        tagsList.value = tagsList.value + tag;
    }
}