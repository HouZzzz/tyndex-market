// cart actions
function addToCartButton() {
    var newInput = document.createElement("input");
    newInput.setAttribute("type", "number");
    newInput.setAttribute("name", "quantity");
    newInput.setAttribute("id", "quantity");
    newInput.setAttribute("onchange", "changeQuantity()");
    newInput.setAttribute("value", "1");

    // Get the parent element of the button
    var parentElement = document.getElementById("addToCartButton").parentNode;
    // Replace the button with the new input element
    parentElement.replaceChild(newInput, document.getElementById("addToCartButton"));

    var productID = document.getElementById("id").value;
    console.log("adding product with id " + productID + " to cart");

    addToCart(1);
}

function changeQuantity() {
    var currentQuantity = document.getElementById("quantity").value;
    if (currentQuantity <= 0) {
        var newInput = document.createElement("input");
        newInput.setAttribute("type", "button");
        newInput.setAttribute("id", "addToCartButton");
        newInput.setAttribute("onclick", "addToCartButton()");
        newInput.setAttribute("value", "Добавить в корзину");

        // Get the parent element of the button
        var parentElement = document.getElementById("quantity").parentNode;
        // Replace the button with the new input element
        parentElement.replaceChild(newInput, document.getElementById("quantity"));

        // delete order
        addToCart(0);
    } else {
        addToCart(currentQuantity);
    }
}

function getUserSecureUUID() {
    const cookieString = document.cookie;
    const cookies = cookieString.split("; ");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].split("=");
      if (cookie[0] == "tyndex") {
        return cookie[1];
      }
    }
    return "";
}

function addToCart(quantity) {
    var id = document.getElementById("id").value;

    const url = '/product/add-to-cart';
    const data = {
        'secure' : getUserSecureUUID(),
        'id' : parseInt(id),
        'quantity' : quantity
    }
    
    sendRequest('POST',url,data);
}


// feedbacks actions
function leaveFeedback() {
    const value = document.getElementById("fb_value").value;
    const text = document.getElementById("fb_text").value;

    const data = {
        'product_id' : parseInt(document.getElementById("id").value),
        'feedback_text' : text,
        'feedback_value' : parseInt(value),
    }

    const status = sendRequest(
        'POST',
        '/feedback',
        data
    );

     // append feedback to list 
    const fbdiv = document.createElement("div");
    fbdiv.classList.add('feedback');
    
    const bname = document.createElement("b");
    // bname.appendChild(document.createTextNode('username'))
    bname.innerHTML = document.getElementById("username").value;
    
    const bvalue = document.createElement("b");
    bvalue.innerHTML = value;
    
    const ptext = document.createElement("p");
    ptext.innerHTML = text;
    
    fbdiv.appendChild(bname);
    fbdiv.appendChild(document.createElement("br"))
    fbdiv.appendChild(bvalue);
    fbdiv.appendChild(ptext);
    
    const fblist = document.getElementById("feedbacks");
    fblist.insertBefore(fbdiv,fblist.firstChild);

    document.getElementById("fb_text").value = "";
}

function displayRangeValue(value) {
    document.getElementById("value_display").innerHTML = value;
}

// other
function redirectToSeller() {
    location.href='/seller/' + document.getElementById("seller_id").value;
}

function sendRequest(method, url,body = null) {
  return new Promise((resolve,reject) => {
      const xhr = new XMLHttpRequest()

      xhr.open(method,'/api' + url)

      xhr.responseType = 'json'
      xhr.setRequestHeader('Content-Type','application/json')

      xhr.onload = () => {
          if (xhr.status >= 400) {
              reject(xhr.response)
              return xhr.status;
          } else {
              resolve(xhr.response)
              return xhr.status;
          }
      }

      xhr.onerror = () => {
          reject(xhr.response)
      }
    console.log('sending request with body ' + JSON.stringify(body));
      xhr.send(JSON.stringify(body))
  })
}
