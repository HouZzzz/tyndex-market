package com.market.Communication;

import com.market.Communication.Entity.Spellcheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Repository
public class SpellcheckCommunication {
    @Autowired
    private RestTemplate restTemplate;
    private String URL = "https://speller.yandex.net/services/spellservice.json/checkText?text=";

    private Logger LOG = LoggerFactory.getLogger(SpellcheckCommunication.class);

    public List<Spellcheck> spell(String text) {
        try {
            ResponseEntity<List<Spellcheck>> response = restTemplate.exchange(
                    new URI(URL + text.replaceAll(" ","+")),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>(){});

            List<Spellcheck> body = response.getBody();
            LOG.debug("spellcheck is " + body);
            return body;
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }
}
