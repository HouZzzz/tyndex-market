package com.market.Communication;

import com.market.Communication.Entity.YandexDictionary.DictionaryWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Repository
public class DictionaryCommunication {
    @Autowired
    private RestTemplate restTemplate;
    private String URL = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?";

    private String apiKey = "dict.1.1.20230421T125342Z.3c5f5280a9fe970f.7b84044b4d875057e3086aade65760f799840c3c";

    private Logger LOG = LoggerFactory.getLogger(DictionaryCommunication.class);

    public DictionaryWrapper getDictionary(String text,String lang) {
        try {
            String args = String.join("&", new String[]{"key=" + apiKey, "lang=" + lang, "text=" + text.replaceAll(" ", "+"), "flags=4"});
            ResponseEntity<DictionaryWrapper> response = restTemplate.exchange(
                    new URI(URL + args),
                    HttpMethod.GET,
                    null,
                    DictionaryWrapper.class);

            DictionaryWrapper body = response.getBody();
            LOG.debug("dictionaryWrapper is " + body);
            return body;
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }
}
