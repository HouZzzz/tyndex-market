package com.market.Communication.Entity;

import java.util.List;

public class Spellcheck {
    int code;
    int pose;
    int row;
    int col;
    int len;
    String word;
    List<String> suggestions;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPose() {
        return pose;
    }

    public void setPose(int pose) {
        this.pose = pose;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public void setS(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    @Override
    public String toString() {
        return "Spellcheck{" +
                "code=" + code +
                ", pose=" + pose +
                ", row=" + row +
                ", col=" + col +
                ", len=" + len +
                ", word='" + word + '\'' +
                ", suggestions=" + suggestions +
                '}';
    }
}
