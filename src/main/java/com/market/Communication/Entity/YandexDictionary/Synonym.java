package com.market.Communication.Entity.YandexDictionary;

public class Synonym {
    private String text;
    private String pos;
    private int fr;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public int getFr() {
        return fr;
    }

    public void setFr(int fr) {
        this.fr = fr;
    }

    public Synonym() {
    }

    @Override
    public String toString() {
        return "Synonym{" +
                "text='" + text + '\'' +
                ", pos='" + pos + '\'' +
                ", fr=" + fr +
                '}';
    }
}
