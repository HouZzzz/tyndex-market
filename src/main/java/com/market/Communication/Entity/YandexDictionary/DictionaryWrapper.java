package com.market.Communication.Entity.YandexDictionary;

import java.util.List;

public class DictionaryWrapper {
    private List<Dictionary> def;

    public List<Dictionary> getDef() {
        return def;
    }

    public void setDef(List<Dictionary> def) {
        this.def = def;
    }

    public DictionaryWrapper() {
    }

    @Override
    public String toString() {
        return "DictionaryWrapper{" +
                "def=" + def +
                '}';
    }
}