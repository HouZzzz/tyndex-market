package com.market.Communication.Entity.YandexDictionary;

import java.util.List;

public class Translation {
    private String text;
    private String post;
    private int fr;
    private List<Synonym> synonyms;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getFr() {
        return fr;
    }

    public void setFr(int fr) {
        this.fr = fr;
    }

    public List<Synonym> getSyn() {
        return synonyms;
    }

    public void setSyn(List<Synonym> synonyms) {
        this.synonyms = synonyms;
    }

    public Translation() {
    }

    @Override
    public String toString() {
        return "Translation{" +
                "text='" + text + '\'' +
                ", post='" + post + '\'' +
                ", fr=" + fr +
                ", synonyms=" + synonyms +
                '}';
    }
}
