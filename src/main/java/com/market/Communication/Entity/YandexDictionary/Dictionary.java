package com.market.Communication.Entity.YandexDictionary;

import java.util.List;

public class Dictionary {
    private String text;
    private String pos;
    private List<Translation> translations;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public List<Translation> getTr() {
        return translations;
    }

    public void setTr(List<Translation> translations) {
        this.translations = translations;
    }

    public Dictionary() {
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "text='" + text + '\'' +
                ", pos='" + pos + '\'' +
                ", translations=" + translations +
                '}';
    }
}
