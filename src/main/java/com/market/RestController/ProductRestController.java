package com.market.RestController;

import com.google.gson.Gson;
import com.market.Annotation.CustomerOptional;
import com.market.Controllers.BodyWrapper.AddToCartWrapper;
import com.market.Controllers.BodyWrapper.LeaveFeedbackWrapper;
import com.market.Entity.Customer;
import com.market.Service.Interfaces.ProductService;
import com.market.Service.Interfaces.UserCookieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ProductRestController {
    @Autowired
    private ProductService productService;

    @Autowired
    private UserCookieService userCookieService;

    @CustomerOptional // todo change to @CustomerRequired
    @PostMapping("/feedback")
    // fixme another buggy params
    public ResponseEntity<Void> leaveFeedback(@RequestBody String body, Customer customer, @CookieValue(name = "tyndex", required = false) String principal) {
        LeaveFeedbackWrapper wrapper = new Gson().fromJson(body, LeaveFeedbackWrapper.class);

        if (customer != null && customer.getId() != 0) {
            productService.saveFeedback(wrapper,customer);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
    }

    @PostMapping("/product/add-to-cart")
    // fixme buggy method. cant fetch params by @RequestParam
    public ResponseEntity<Void> addToCart(@RequestBody String body) {
        AddToCartWrapper wrapper = new Gson().fromJson(body, AddToCartWrapper.class);
        String secure = wrapper.getSecure();
        int id = wrapper.getId();
        int quantity = wrapper.getQuantity();

        Object user = userCookieService.getUserBySessionUUID(secure);
        if (user instanceof Customer customer) {
            productService.addProductToCart(
                    quantity,
                    id,
                    customer);
        }
        return ResponseEntity.ok().build();
    }
}
