package com.market.DAO.Interfaces;

import com.market.Entity.*;

import java.util.List;

public interface ProductDAO {
    void saveProduct(Product product);
    Product getProductByID(int id);
    List<Product> getProductsBySellerID(int id);

    List<Product> getAllProducts();

    List<Product> getProductsByCategory(Category category);
    List<Product> getProductsByCategoryWithLimit(Category category, int limit);

    List<Product> getProductsWithWord(String word);

    void saveVisit(Visit visit);

    List<Visit> getUserVisitsHistory(int userID);

    List<Visit> getVisitByProductID(int id);

    void saveSearch(SearchHistory search);

    List<SearchHistory> getUserSearchHistory(int userID);

    void saveOrder(Order order);
    void deleteOrder(Order order);
    Order getOrderByCustomerAndProdcut(int customerID, int productID);

    void saveFeedback(Feedback feedback);
    List<Feedback> getFeedbacksByProduct(Product product);
    List<Feedback> getFeedbacksBySeller(Seller seller);

    List<Tag> getTagsByWord(String word);
}
