package com.market.DAO.Interfaces;

import com.market.Entity.Session;

public interface SessionDAO {
    void saveSession(Session session);
    Session getSessionByUUID(String uuid);
    void deleteSessionByUUID(String uuid);
}
