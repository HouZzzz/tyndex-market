package com.market.DAO.Interfaces;

import com.market.Entity.Seller;

public interface SellerDAO {
    void saveSeller(Seller seller);
    Seller getSellerByTitle(String title);
    Seller getSellerByID(int id);
}
