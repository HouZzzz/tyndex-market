package com.market.DAO.Interfaces;

import com.market.Entity.Category;

import java.util.List;

public interface CategoryDAO {
    List<Category> getAllCategories();
}
