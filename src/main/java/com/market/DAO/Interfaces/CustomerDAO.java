package com.market.DAO.Interfaces;

import com.market.Entity.Customer;

public interface CustomerDAO {
    void saveCustomer(Customer customer);
    Customer getCustomerByEmail(String email);

    Customer getCustomerByID(int id);

}
