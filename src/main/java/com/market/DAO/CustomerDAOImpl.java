package com.market.DAO;

import com.market.DAO.Interfaces.CustomerDAO;
import com.market.Entity.Customer;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(CustomerDAO.class);


    @Override
    public void saveCustomer(Customer customer) {
        LOG.debug("saving or updating customer " + customer);
        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(customer);
    }

    @Override
    public Customer getCustomerByEmail(String email) {
        LOG.debug("finding customer by email " + email);

        // will contain 1 or 0 objects
        List<Customer> customers = sessionFactory
                .getCurrentSession()
                .createQuery("from Customer where email = '%s'".formatted(email), Customer.class)
                .getResultList();

        LOG.debug("found customer with email " + email + ": " + customers);
        return customers.size() > 0 ? customers.get(0) : null;
    }

    @Override
    public Customer getCustomerByID(int id) {
        return sessionFactory
                .getCurrentSession()
                .get(Customer.class,id);
    }
}
