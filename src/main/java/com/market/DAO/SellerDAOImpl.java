package com.market.DAO;

import com.market.DAO.Interfaces.SellerDAO;
import com.market.Entity.Seller;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SellerDAOImpl implements SellerDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(SellerDAO.class);


    @Override
    public void saveSeller(Seller customer) {
        LOG.debug("saving or updating seller " + customer);
        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(customer);
    }

    @Override
    public Seller getSellerByTitle(String title) {
        LOG.debug("finding seller by title " + title);

        // will contain 1 or 0 objects
        List<Seller> sellers = sessionFactory
                .getCurrentSession()
                .createQuery("from Seller where title = '%s'".formatted(title), Seller.class)
                .getResultList();

        LOG.debug("found seller with title " + title + ": " + sellers);
        return sellers.size() > 0 ? sellers.get(0) : null;
    }

    @Override
    public Seller getSellerByID(int id) {
        LOG.debug("finding seller by id " + id);

        // will contain 1 or 0 objects
        List<Seller> sellers = sessionFactory
                .getCurrentSession()
                .createQuery("from Seller where id = %d".formatted(id), Seller.class)
                .getResultList();

        LOG.debug("found seller with id " + id + ": " + sellers);
        return sellers.size() > 0 ? sellers.get(0) : null;
    }
}
