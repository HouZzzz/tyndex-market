package com.market.DAO;

import com.market.DAO.Interfaces.CategoryDAO;
import com.market.Entity.Category;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDAOImpl implements CategoryDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(CategoryDAO.class);

    @Override
    public List<Category> getAllCategories() {
        List<Category> categories = sessionFactory
                .getCurrentSession()
                .createQuery("from Category ", Category.class)
                .getResultList();
        LOG.debug("found list of categories: " + categories);
        return categories;
    }
}
