package com.market.DAO;

import com.market.DAO.Interfaces.SessionDAO;
import com.market.Entity.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SessionDAOImpl implements SessionDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(SessionDAO.class);
    @Override
    public void saveSession(Session session) {
        LOG.debug("saving session " + session);
        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(session);
    }

    @Override
    public Session getSessionByUUID(String uuid) {
        // will contain only one or zero sessions
        List<Session> sessions = sessionFactory
                .getCurrentSession()
                .createQuery("from Session where uuid = '%s'".formatted(uuid), Session.class)
                .getResultList();
        LOG.debug("found session by uuid " + uuid + ": " + sessions);
        return sessions.size() > 0 ? sessions.get(0) : null;
    }

    @Override
    public void deleteSessionByUUID(String uuid) {
        LOG.debug("deleting session with uuid " + uuid);
        org.hibernate.Session currentSession = sessionFactory
                .getCurrentSession();
        Session session = currentSession.get(Session.class, uuid);
        if (session != null) currentSession.delete(session);
    }
}
