package com.market.DAO;

import com.market.DAO.Interfaces.ProductDAO;
import com.market.Entity.*;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductDAOImpl implements ProductDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(ProductDAO.class);

    @Override
    public void saveProduct(Product product) {
        LOG.debug("saving product " + product);
        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(product);
    }

    @Override
    public Product getProductByID(int id) {
        Product product = sessionFactory
                .getCurrentSession()
                .get(Product.class, id);
        LOG.debug("found product by id " + id + ": " + product);
        return product;
    }

    @Override
    public List<Product> getProductsBySellerID(int id) {
        List<Product> products = sessionFactory
                .getCurrentSession()
                .createQuery("from Product where seller.id = " + id, Product.class)
                .getResultList();
        LOG.debug("found list of products with seller id " + id + ": " + products);
        return products;
    }

    @Override
    public List<Product> getAllProducts() {
        return sessionFactory
                .getCurrentSession()
                .createQuery("from Product",Product.class)
                .getResultList();
    }

    @Override
    public void saveVisit(Visit visit) {
        List<Visit> visitsWithSameProduct = sessionFactory
                .getCurrentSession()
                .createQuery("from Visit where product.id = " + visit.getProduct().getId() + " and customer.id = " + visit.getCustomer().getId(), Visit.class)
                .getResultList();
        if (visitsWithSameProduct.size() == 0) {
            sessionFactory
                    .getCurrentSession()
                    .persist(visit);
        }
    }

    @Override
    public List<Visit> getUserVisitsHistory(int userID) {
        List<Visit> visits = sessionFactory
                .getCurrentSession()
                .createQuery("from Visit where customer.id = " + userID, Visit.class)
                .getResultList();
        LOG.debug("found list of visits of user " + userID + ": " + visits);
        return visits;
    }

    @Override
    public List<Visit> getVisitByProductID(int id) {
        List<Visit> visits = sessionFactory
                .getCurrentSession()
                .createQuery("from Visit where product.id = " + id, Visit.class)
                .getResultList();
        LOG.trace("found list of visits of products with id " + id + ": " + visits);
        return visits;
    }

    @Override
    public List<Product> getProductsByCategory(Category category) {
        List<Product> products = sessionFactory
                .getCurrentSession()
                .createNativeQuery("SELECT * FROM product where category_id = " + category.getId(), Product.class)
                .getResultList();
        LOG.debug("found list of products with category " + category + ": " + products);
        return products;
    }

    @Override
    public List<Product> getProductsByCategoryWithLimit(Category category, int limit) {
        List<Product> products = sessionFactory
                .getCurrentSession()
                .createNativeQuery("SELECT * FROM product where category_id = " + category.getId() + " LIMIT " + limit, Product.class)
                .getResultList();
        LOG.debug("found list of products with category " + category + " and limit " + limit + ": " + products);
        return products;
    }

    @Override
    public List<Product> getProductsWithWord(String word) {
        List<Product> products = sessionFactory
                .getCurrentSession()
                .createQuery("from Product where LOWER(name) LIKE LOWER('%" + word + "%') OR " +
                        "LOWER(description) LIKE LOWER('%" + word + "%') OR " +
                        "LOWER(seller.title) LIKE LOWER('%" + word + "%')", Product.class)
                .getResultList();
        LOG.trace("found list of products with word " + word + ": " + products);
        return products;
    }

    @Override
    public void saveSearch(SearchHistory search) {
        LOG.debug("saving search " + search);
        sessionFactory
                .getCurrentSession()
                .persist(search);
    }

    @Override
    public List<SearchHistory> getUserSearchHistory(int userID) {
        List<SearchHistory> searchHistory = sessionFactory
                .getCurrentSession()
                .createQuery("from SearchHistory where customer.id = " + userID, SearchHistory.class)
                .getResultList();
        LOG.debug("found search history from user " + userID + ": " + searchHistory);
        return searchHistory;
    }

    @Override
    public void saveOrder(Order order) {
        LOG.debug("saving order " + order);
        sessionFactory
                .getCurrentSession()
                .persist(order);
    }

    @Override
    public void deleteOrder(Order order) {
        sessionFactory
                .getCurrentSession()
                .delete(order);
    }

    @Override
    public Order getOrderByCustomerAndProdcut(int customerID, int productID) {
        // contain 0 or 1 order
        List<Order> order = sessionFactory
                .getCurrentSession()
                .createQuery("from Order where customer.id = %d and product.id = %d".formatted(customerID, productID), Order.class)
                .getResultList();
        LOG.debug("found order by customer id %d and product id %d: %s".formatted(customerID,productID,order));
        return order.isEmpty() ? null : order.get(0);
    }

    @Override
    public void saveFeedback(Feedback feedback) {
        sessionFactory
                .getCurrentSession()
                .persist(feedback);
    }

    @Override
    public List<Feedback> getFeedbacksByProduct(Product product) {
        List<Feedback> feedbacks = sessionFactory
                .getCurrentSession()
                .createQuery("from Feedback where product.id = " + product.getId(), Feedback.class)
                .getResultList();
        LOG.debug("found list of feedbacks by product id " + product.getId() + ": " + feedbacks);
        return feedbacks;
    }

    @Override
    public List<Feedback> getFeedbacksBySeller(Seller seller) {
        List<Feedback> feedbacks = sessionFactory
                .getCurrentSession()
                .createQuery("from Feedback where product.seller.id = " + seller.getId(), Feedback.class)
                .getResultList();
        LOG.debug("found list of feedbacks by seller id " + seller.getId() + ": " + feedbacks);
        return feedbacks;
    }

    @Override
    public List<Tag> getTagsByWord(String word) {
        List<Tag> tags = sessionFactory
                .getCurrentSession()
                .createQuery("from Tag where tag = '" + word + "'", Tag.class)
                .getResultList();
        LOG.trace("found list of tags by word " + word + ": " + tags);
        return tags;
    }
}
