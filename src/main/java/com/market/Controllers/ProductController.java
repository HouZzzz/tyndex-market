package com.market.Controllers;

import com.google.gson.Gson;
import com.market.Annotation.CustomerOptional;
import com.market.Annotation.SellerRequired;
import com.market.Controllers.BodyWrapper.AddToCartWrapper;
import com.market.Controllers.BodyWrapper.LeaveFeedbackWrapper;
import com.market.Entity.Customer;
import com.market.Entity.Product;
import com.market.Entity.Seller;
import com.market.Entity.Tag;
import com.market.Service.Interfaces.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Map;

@Controller
public class ProductController {
    @Autowired
    private UserCookieService userCookieService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private SellerService sellerService;

    private Logger LOG = LoggerFactory.getLogger(ProductController.class);

    @GetMapping("/product/create")
    @SellerRequired
    public String createProdcut(Model model, Seller seller, @CookieValue(name = "tyndex",required = false) String principal) {
//        LOG.debug("enter to create product page with principal " + principal);
//        Seller seller = (Seller) userCookieService.getUserBySessionUUID(principal);
        LOG.debug("enter to create product page with seller account " + seller);

        model.addAttribute("title",seller.getTitle());
        model.addAttribute("description",seller.getDescription());
        model.addAttribute("categories",categoryService.getAllCategories());

        return "create-product";
    }

    @PostMapping("/product/create")
    @SellerRequired
    public String saveProduct(Model model, @ModelAttribute @Valid Product product, BindingResult bindingResult, Seller seller, @CookieValue("tyndex") String principal) {
        if (bindingResult.hasErrors()) {
            validationService.addErrorsToModel(model, bindingResult);

            model.addAttribute("title",seller.getTitle());
            model.addAttribute("description",seller.getDescription());
            model.addAttribute("categories",categoryService.getAllCategories());
            return "create-product";
        }
//        Seller seller = (Seller) userCookieService.getUserBySessionUUID(principal);
        product.setTags(
                Arrays.stream(product.getTags_list().split(" "))
                        .map(t -> new Tag(t,product))
                        .toList());
        product.setSeller(seller);

        productService.saveProduct(product);
        return "redirect:/me";
    }

    @GetMapping("/product/{id}")
    public String displayProduct(@PathVariable int id, Model model, @CookieValue("tyndex") String principal) {
        Object user = userCookieService.getUserBySessionUUID(principal);
        if (user instanceof Customer customer) {
            model.addAttribute("username",customer.getName());
            new Thread(() ->
                    productService.saveVisit(id,customer.getId())
            ).start();
        }
        Product product = productService.getProductByID(id);
        model.addAttribute("feedbacks",productService.getFeedbacksByProduct(product));
        model.addAttribute("product", product);
        model.addAttribute("seller", product.getSeller());
        model.addAttribute("avg_value",sellerService.calculateAverageValue(product.getSeller().getId()));
        return "product-page";
    }

    @CustomerOptional
    @GetMapping("/search")
    public String search(Model model, @RequestParam String query, Customer customer, @CookieValue(name = "tyndex", required = false) String principal) {
        String spelledQuery = searchService.spellQuery(query);
        model.addAttribute("products",searchService.search(query,spelledQuery,customer));
        model.addAttribute("query",spelledQuery.isEmpty() ? query : spelledQuery);
        return "main";
    }
}
