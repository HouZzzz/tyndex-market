package com.market.Controllers;

import com.market.Entity.Customer;
import com.market.Entity.Seller;
import com.market.Service.Interfaces.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class UserController {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserCookieService userCookieService;

    @Autowired
    private SellerService sellerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private VerifyService verifyService;

    @Autowired
    private SessionService sessionService;

    private Logger LOG = LoggerFactory.getLogger(UserController.class);


    @GetMapping("/register")
    public String registerAs() {
        return "register-as";
    }

    @GetMapping("/register/seller")
    public String registerAsSeller() {
        return "register-as-seller";
    }

    @GetMapping("/register/buyer")
    public String registerAsBuyer() {
        return "register-as-buyer";
    }

    @GetMapping("/verify-user")
    public String verifyUser(Model model,
                             @CookieValue(value = "tyndex",required = false) String principal,
                             @RequestParam(required = false) String name,
                             @RequestParam(required = false) String title,
                             @RequestParam(required = false) String description,
                             @RequestParam String email,
                             @RequestParam String type,
                             HttpServletResponse response) {
        LOG.debug("verifying user with email " + email);

        Cookie cookie = userCookieService.generateVerifyToken(email);
        response.addCookie(cookie);

        //kill session
        if (principal != null && !principal.isEmpty()) {
            response.addCookie(userCookieService.killCookie("tyndex"));
            sessionService.deleteSessionByUUID(principal);
        }

        verifyService.startVerify(title, email,name,cookie.getValue());

        model.addAttribute("email",email);
        model.addAttribute("name",name);
        model.addAttribute("title",title);
        model.addAttribute("description",description);
        model.addAttribute("type",type);
        return "verify-login";
    }

    @PostMapping("/verify")
    public String verifyProcess(@RequestParam String code,
                                @CookieValue("tyndex-verify") String verifyToken,
                                @RequestParam(required = false) String name,
                                @RequestParam(required = false) String title,
                                @RequestParam(required = false) String description,
                                @RequestParam String email,
                                @RequestParam String type,
                                HttpServletResponse response) {

        boolean isVerified = verifyService.verify(
                verifyToken,
                email,
                title,
                name,
                code);

        if (isVerified) {
            boolean userIsCustomer = (name != null && !name.isBlank()) && (title == null || title.isEmpty());
            boolean userIsSeller = (title != null && !title.isBlank()) && (name == null || name.isEmpty());

            // user is customer
            if (userIsCustomer) {
                // register customer
                if (type.equals("register")) {
                    Customer customer = new Customer();
                    customer.setEmail(email);
                    customer.setName(name);
                    customerService.registerCustomer(customer);
                }
            } else if (userIsSeller) {
                LOG.debug("description is " + description);
                // register seller
                if (type.equals("register")) {
                    Seller seller = new Seller();
                    seller.setEmail(email);
                    seller.setDescription(description);
                    seller.setTitle(title);
                    sellerService.registerSeller(seller);
                }
            }

            Cookie cookie;
            if (userIsCustomer) {
                cookie = userCookieService.generateSession(email, null);
            } else {
                cookie = userCookieService.generateSession(null,title);
            }
            response.addCookie(cookie);
        }

        response.addCookie(userCookieService.killCookie("tyndex-verify"));

        return "redirect:/";
    }


    @PostMapping("/register-buyer")
    public String registerBuyer(@ModelAttribute Customer customer, HttpServletResponse response) {
        customerService.registerCustomer(customer);
        response.addCookie(userCookieService.generateSession(customer.getEmail(),null));
        return "redirect:/";
    }

    @PostMapping("/register-seller")
    public String registerSeller(@ModelAttribute Seller seller, HttpServletResponse response) {
        sellerService.registerSeller(seller);
        response.addCookie(userCookieService.generateSession(null,seller.getTitle()));
        return "redirect:/";
    }


    @GetMapping("/me")
    public String myAccountPage(Model model, @CookieValue(value = "tyndex", required = false) String principal) {
        LOG.debug("enter in account page with principal " + principal);

        // user not authenticated
        if (principal == null) {
            LOG.debug("rejected: not authenticated");
            return "redirect:/";
        }

        Object user = userCookieService.getUserBySessionUUID(principal);

        // user is seller
        if (user instanceof Seller seller) {
            model.addAttribute("title",seller.getTitle());
            model.addAttribute("description",seller.getDescription());
            model.addAttribute("products",productService.getProductsBySellerID(seller.getId()));

            return "seller-account";
        } else if (user instanceof Customer customer) { // user in not seller (customer)
            model.addAttribute("name", customer.getName());
            model.addAttribute("email",customer.getEmail());

            return "customer-account";
        }

        LOG.debug("user is not instance of seller or customer");
        return "redirect:/";
    }

    @GetMapping("/login/seller")
    public String loginAsSeller() {
        return "login-as-seller";
    }

    @PostMapping("/login-seller")
    public String loginAsSellerPost(@RequestParam String title, HttpServletResponse response) {
        LOG.debug("logging in as seller with title " + title);

        Cookie cookie = userCookieService.generateSession(null, title);
        response.addCookie(cookie);
        return "redirect:/";
    }

    @GetMapping("/login/buyer")
    public String loginAsBuyer() {
        return "login-as-buyer";
    }

    @PostMapping("/login-buyer")
    public String loginAsBuyerPost(@RequestParam String email, HttpServletResponse response) {
        LOG.debug("logging in as seller with title " + email);

        Cookie cookie = userCookieService.generateSession(email, null);
        response.addCookie(cookie);
        return "redirect:/";
    }

    @GetMapping("/seller/{id}")
    public String sellerPage(@PathVariable int id, Model model) {
        // fixme do smth if seller is null
        Seller seller = sellerService.getSellerByID(id);
        // todo add products field to seller object
        model.addAttribute("seller",seller);
        model.addAttribute("products",productService.getProductsBySellerID(id));
        model.addAttribute("average",sellerService.calculateAverageValue(id));
        return "seller-page";
    }
}
