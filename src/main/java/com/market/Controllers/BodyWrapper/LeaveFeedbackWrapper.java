package com.market.Controllers.BodyWrapper;

public class LeaveFeedbackWrapper {
    private int product_id;
    private String feedback_text;
    private int feedback_value;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getFeedback_text() {
        return feedback_text;
    }

    public void setFeedback_text(String feedback_text) {
        this.feedback_text = feedback_text;
    }

    public int getFeedback_value() {
        return feedback_value;
    }

    public void setFeedback_value(int feedback_value) {
        this.feedback_value = feedback_value;
    }

    @Override
    public String toString() {
        return "LeaveFeedbackWrapper{" +
                "product_id=" + product_id +
                ", feedback_text='" + feedback_text + '\'' +
                ", feedback_value=" + feedback_value +
                '}';
    }
}
