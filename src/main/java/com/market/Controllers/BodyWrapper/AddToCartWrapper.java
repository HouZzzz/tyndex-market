package com.market.Controllers.BodyWrapper;

public class AddToCartWrapper {
    private String secure;
    private int id;
    private int quantity;

    public String getSecure() {
        return secure;
    }

    public void setSecure(String secure) {
        this.secure = secure;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "AddToCartWrapper{" +
                "secure='" + secure + '\'' +
                ", id=" + id +
                ", quantity=" + quantity +
                '}';
    }
}
