package com.market.Controllers;

import com.market.Annotation.SellerRequired;
import com.market.Entity.Customer;
import com.market.Entity.Seller;
import com.market.Service.Interfaces.ProductService;
import com.market.Service.Interfaces.RecommendationService;
import com.market.Service.Interfaces.UserCookieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @Autowired
    private UserCookieService userCookieService;

    @Autowired
    private RecommendationService recommendationService;

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public String mainPage(Model model, @CookieValue(value = "tyndex",required = false) String principal) {
        String text = "Привет, новичок";
        boolean autheticated = false;

        if (principal != null) {
            Object user = userCookieService.getUserBySessionUUID(principal);
            if (user == null) {
                model.addAttribute("text",text);
                return "main";
            }

            boolean isCustomer = user instanceof Customer;
            if (isCustomer) {
                model.addAttribute("products",recommendationService.getRecommendedProducts(((Customer) user).getId()));
                autheticated = true;
            }

            text = isCustomer ? ((Customer) user).getName() : ((Seller) user).getTitle();
            text += isCustomer ? ", вы покупатель" : ", вы продавец";
        }

        model.addAttribute("text",text);
        if (!autheticated) model.addAttribute("products",productService.getAllProducts());
        return "main";
    }
}
