package com.market.Service;

import com.market.DAO.Interfaces.ProductDAO;
import com.market.Entity.Category;
import com.market.Entity.Product;
import com.market.Entity.SearchHistory;
import com.market.Entity.Visit;
import com.market.Service.Interfaces.RecommendationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecommendationServiceImpl implements RecommendationService {
    @Autowired
    private ProductDAO productDAO;

    private Logger LOG = LoggerFactory.getLogger(RecommendationService.class);

    @Override
    @Transactional
    public Set<Product> getRecommendedProducts(int id) {
        LOG.debug("creating recommendations for user with id " + id);
        Set<Product> products = new HashSet<>();

        List<SearchHistory> searchHistory = productDAO.getUserSearchHistory(id);

        for (SearchHistory search : searchHistory) {
            for (String word : search.getQuery().split(" ")) {
                products.addAll(productDAO.getProductsWithWord(word));
            }
        }

        Set<Product> visitedProducts = productDAO.getUserVisitsHistory(id)
                .stream()
                .map(Visit::getProduct)
                .collect(Collectors.toSet());

        Map<Category, Double> categoriesRatio = getCategoriesRatio(visitedProducts);

        for (Category category : categoriesRatio.keySet()) {
            List<Product> productsByCategory = productDAO.getProductsByCategoryWithLimit(category, (int) (products.size() * categoriesRatio.get(category)));
            if (!productsByCategory.isEmpty()) {
                products.addAll(productsByCategory);
            }
        }

        //sort by visits counter
        products = products.stream().sorted(this::compareVisitsCounted).collect(Collectors.toCollection(LinkedHashSet::new));

        LOG.debug("list of recommended products: " + products);
        if (products.isEmpty()) {
            products.addAll(productDAO.getAllProducts());
        }
        return products;
    }

    @Override
    public int compareVisitsCounted(Product p1, Product p2) {
        return productDAO.getVisitByProductID(p2.getId()).size() - productDAO.getVisitByProductID(p1.getId()).size();
    }

    @Override
    public Map<Category, Double> getCategoriesRatio(Set<Product> products) {
        // V is %
        Map<Category,Double> categoriesRatio = new HashMap<>();

        Map<Category, Long> counts = products.
                stream()
                .map(Product::getCategory)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        LOG.debug("categories counts is " + counts);

        long total = products.size();
        LOG.debug("total is " + total + " products");

        for (Category category : counts.keySet()) {
            double chance = (double) counts.get(category) / (double) total;
            categoriesRatio.put(category, chance);
        }

        LOG.debug("returning list of categories ratio " + categoriesRatio);
        return categoriesRatio;
    }
}
