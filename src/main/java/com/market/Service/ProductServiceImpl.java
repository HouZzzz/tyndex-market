package com.market.Service;

import com.market.Controllers.BodyWrapper.LeaveFeedbackWrapper;
import com.market.DAO.Interfaces.CategoryDAO;
import com.market.DAO.Interfaces.CustomerDAO;
import com.market.DAO.Interfaces.ProductDAO;
import com.market.Entity.*;
import com.market.Service.Interfaces.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private CustomerDAO customerDAO;

    private Logger LOG = LoggerFactory.getLogger(ProductService.class);

    @Override
    public void saveProduct(Product product) {
        Inventory inventory = new Inventory(product.getStockLevel());
        inventory.setProduct(product);
        product.setInventory(inventory);

        product.setAvailable(product.getStockLevel() > 0);

        Category category = categoryDAO
                .getAllCategories()
                .stream()
                .filter(c -> c.getName().equals(product.getCategory().getName()))
                .findFirst()
                .get();
        product.setCategory(category);

        productDAO.saveProduct(product);
    }

    @Override
    public Product getProductByID(int id) {
        return productDAO.getProductByID(id);
    }

    @Override
    public List<Product> getProductsBySellerID(int id) {
        return productDAO.getProductsBySellerID(id);
    }

    @Override
    @Transactional
    public List<Product> getAllProducts() {
        List<Product> allProducts = productDAO.getAllProducts();
        LOG.debug("found list of products: " + allProducts);
        return allProducts;
    }

    @Override
    @Transactional
    public void saveVisit(int productID, int customer_id) {
        Visit visit = new Visit();
        visit.setProduct(productDAO.getProductByID(productID));
        visit.setCustomer(customerDAO.getCustomerByID(customer_id));

        LOG.debug("created visit entity " + visit);
        productDAO.saveVisit(visit);
    }

    @Override
    @Transactional
    public void addProductToCart(int quantity, int productID, Customer customer) {
        Order existingOrder = productDAO.getOrderByCustomerAndProdcut(customer.getId(), productID);
        if (existingOrder == null) {
            Order order = new Order();
            Product product = productDAO.getProductByID(productID);
            order.setCustomer(customer);
            order.setQuantity(quantity);
            order.setProduct(product);
            order.setTotalPrice(product.getPrice() * quantity);

            productDAO.saveOrder(order);
        } else {
            if (quantity > 0) {
                existingOrder.setQuantity(quantity);
                existingOrder.setTotalPrice(existingOrder.getProduct().getPrice() * quantity);

                productDAO.saveOrder(existingOrder);
            } else {
                productDAO.deleteOrder(existingOrder);
            }
        }
    }

    @Override
    @Transactional
    public void saveFeedback(LeaveFeedbackWrapper wrapper, Customer customer) {
        Feedback feedback = new Feedback();
        feedback.setProduct(productDAO.getProductByID(wrapper.getProduct_id()));
        feedback.setCustomer(customer);
        feedback.setText(wrapper.getFeedback_text());
        feedback.setValue(wrapper.getFeedback_value());

        productDAO.saveFeedback(feedback);
    }

    @Override
    @Transactional
    public List<Feedback> getFeedbacksByProduct(Product product) {
        return productDAO.getFeedbacksByProduct(product);
    }

    @Override
    @Transactional
    public List<Feedback> getFeedbacksBySeller(Seller seller) {
        return productDAO.getFeedbacksBySeller(seller);
    }
}
