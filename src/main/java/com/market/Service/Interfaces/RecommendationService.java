package com.market.Service.Interfaces;

import com.market.Entity.Category;
import com.market.Entity.Product;

import java.util.Map;
import java.util.Set;

public interface RecommendationService {
    Set<Product> getRecommendedProducts(int id);
    int compareVisitsCounted(Product p1, Product p2);

    Map<Category, Double> getCategoriesRatio(Set<Product> products);
}
