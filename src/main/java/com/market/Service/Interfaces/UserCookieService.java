package com.market.Service.Interfaces;

import com.market.Entity.Customer;
import com.market.Entity.Seller;

import javax.servlet.http.Cookie;

public interface UserCookieService {
    Cookie generateSession(String email,String title);
    Object getUserBySessionUUID(String uuid);

    Cookie generateVerifyToken(String email);

    Cookie killCookie(String cookieName);
}
