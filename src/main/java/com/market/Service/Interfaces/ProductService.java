package com.market.Service.Interfaces;

import com.market.Controllers.BodyWrapper.LeaveFeedbackWrapper;
import com.market.Entity.Customer;
import com.market.Entity.Feedback;
import com.market.Entity.Product;
import com.market.Entity.Seller;

import java.util.List;

public interface ProductService {
    void saveProduct(Product product);
    Product getProductByID(int id);
    List<Product> getProductsBySellerID(int id);

    List<Product> getAllProducts();
    void saveVisit(int productID, int customer_id);

    void addProductToCart(int quantity, int productID, Customer customer);

    void saveFeedback(LeaveFeedbackWrapper wrapper, Customer customer);
    List<Feedback> getFeedbacksByProduct(Product product);
    List<Feedback> getFeedbacksBySeller(Seller seller);
}
