package com.market.Service.Interfaces;

public interface MailNotificationService {
    public void sendMail(String to,String subject,String body);
}
