package com.market.Service.Interfaces;

public interface VerifyService {
    String generateVerifyCode(int length);
    String generateVerifyCode();
    boolean verify(String token, String email, String title, String name, String code);
    void startVerify(String title, String email,String name, String token);
}
