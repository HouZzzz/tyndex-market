package com.market.Service.Interfaces;

import com.market.Entity.Category;
import com.market.Entity.Customer;
import com.market.Entity.Product;

import java.util.List;
import java.util.Set;

public interface SearchService {

    Set<Product> search(String query, String spelledQuery, Customer customer);

    List<Category> calculateMostPopularCategory(Set<Product> products);

    String spellQuery(String query);

    List<String> generateTags(String query);

    int compareEquality(Product product1, Product product2, String query);
}
