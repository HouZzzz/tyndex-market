package com.market.Service.Interfaces;

import com.market.Entity.Seller;

public interface SellerService {
    void registerSeller(Seller customer);
    Seller getSellerByTitle(String title);
    Seller getSellerByID(int id);

    String calculateAverageValue(int sellerID);
}
