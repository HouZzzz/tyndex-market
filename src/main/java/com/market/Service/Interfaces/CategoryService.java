package com.market.Service.Interfaces;

import com.market.Entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();
}
