package com.market.Service.Interfaces;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

public interface ValidationService {
    void addErrorsToModel(Model model, BindingResult bindingResult);
}
