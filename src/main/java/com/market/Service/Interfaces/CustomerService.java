package com.market.Service.Interfaces;

import com.market.Entity.Customer;

import java.util.List;

public interface CustomerService {
    void registerCustomer(Customer customer);
    Customer getCustomerByEmail(String email);

}
