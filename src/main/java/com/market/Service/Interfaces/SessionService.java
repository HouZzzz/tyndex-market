package com.market.Service.Interfaces;

import com.market.Entity.Session;

public interface SessionService {
    void saveSession(Session session);
    Session getSessionByUUID(String uuid);
    void deleteSessionByUUID(String uuid);
}
