package com.market.Service;

import com.market.DAO.Interfaces.SessionDAO;
import com.market.Entity.Session;
import com.market.Service.Interfaces.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SessionServiceImpl implements SessionService {
    @Autowired
    private SessionDAO sessionDAO;

    @Override
    @Transactional
    public void saveSession(Session session) {
        sessionDAO.saveSession(session);
    }

    @Override
    @Transactional
    public Session getSessionByUUID(String uuid) {
        return sessionDAO.getSessionByUUID(uuid);
    }

    @Override
    @Transactional
    public void deleteSessionByUUID(String uuid) {
        sessionDAO.deleteSessionByUUID(uuid);
    }
}
