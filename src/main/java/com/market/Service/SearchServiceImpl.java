package com.market.Service;

import com.market.Communication.DictionaryCommunication;
import com.market.Communication.Entity.Spellcheck;
import com.market.Communication.Entity.YandexDictionary.Dictionary;
import com.market.Communication.Entity.YandexDictionary.Synonym;
import com.market.Communication.Entity.YandexDictionary.Translation;
import com.market.Communication.SpellcheckCommunication;
import com.market.DAO.Interfaces.CustomerDAO;
import com.market.DAO.Interfaces.ProductDAO;
import com.market.Entity.*;
import com.market.Service.Interfaces.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private SpellcheckCommunication spellcheckCommunication;

    @Autowired
    private DictionaryCommunication dictionaryCommunication;

    private Logger LOG = LoggerFactory.getLogger(SearchService.class);


    @Override
    @Transactional
    public Set<Product> search(String query, String spelledQuery, Customer customer) {

        Set<Product> products = new HashSet<>();

        // merge query with speller query
        String mergedQuery = Arrays.stream((query + " " + spelledQuery).split(" "))
                .distinct()
                .collect(Collectors.joining(" "));

        // add products with each word from query
        for (String word : mergedQuery.split(" ")) {
            if (word.length() > 2) {
                List<Product> productsWithWord = productDAO.getProductsWithWord(word);
                if (productsWithWord.size() > 0) products.addAll(productsWithWord);
            }
        }

        // sort
        products = products.stream().sorted((p2,p1) -> compareEquality(p1,p2,mergedQuery)).collect(Collectors.toCollection(LinkedHashSet::new));

        // add products with synonyms words
        List<String> tags = generateTags(mergedQuery);
        if (!tags.isEmpty()) {
            int charnum = tags.get(0).charAt(0);
            String originalLanguage =
                    (charnum >= 1072 && charnum <= 1103) ? "ru" : "en";

            String tagTranslation = "";
            // get translation
            for (String tag : tags) {
                int fl = tag.toLowerCase().charAt(0);
                if ((fl >= 1072 && fl <= 1103) && originalLanguage.equals("en")) {
                    tagTranslation = tag;
                    LOG.debug("detected translation of tag: " + tagTranslation);
                    break;
                } else if ((fl >= 97 && fl <= 122) && originalLanguage.equals("ru")) {
                    tagTranslation = tag;
                    LOG.debug("detected translation of tag: " + tagTranslation);
                    break;
                }
            }

            List<String> splitedQuery = Arrays.stream(mergedQuery.split(" ")).toList();
            List<String> tagsToRemove = new ArrayList<>();
            for (String tag : tags) {
                if (tag.length() > 2) {

                    boolean contains = false;
                    if (!tag.equals(tagTranslation)) {
                        for (String word : splitedQuery) {
                            if (tag.contains(word) || (!tagTranslation.isEmpty() && tag.contains(tagTranslation))) {
                                contains = true;
                                tagsToRemove.add(tag);
                                break;
                            }
                        }
                    }

                    if (!contains) {
                        List<Product> productsWithWord = productDAO.getProductsWithWord(tag);
                        if (productsWithWord.size() > 0) products.addAll(productsWithWord);

                        List<Tag> tagsList = productDAO.getTagsByWord(tag);
                        if (!tagsList.isEmpty()) {
                            for (Tag tagObj : tagsList) products.add(tagObj.getProduct());
                        }
                    }
                }
            }
            for (String tag : tagsToRemove) tags.remove(tag);

            // calculate categories and add other products to list
            for (Category category : calculateMostPopularCategory(products)) {
                List<Product> productsByCategory = productDAO.getProductsByCategory(category);
                if (productsByCategory.size() > 0) products.addAll(productsByCategory);
            }

            LOG.debug("search result: " + products);
            if (customer.getId() > 0) {
                SearchHistory search = new SearchHistory();
                search.setCustomer(customer);

                String queryToSave = splitedQuery.stream().collect(Collectors.joining(" ")) + " " + tags.stream().collect(Collectors.joining(" "));

                search.setQuery(queryToSave);
                productDAO.saveSearch(search);
            }
        }
        return products;
    }

    @Override
    public List<Category> calculateMostPopularCategory(Set<Product> products) {
        Map<Category, Long> counts = products.stream()
                .map(p -> p.getCategory())
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        LOG.debug("calculated count of categories in list of products: " + counts);
        if (counts.isEmpty()) return new ArrayList<>();
        Long maxValue = counts.values().stream().max(Long::compare).get();
        LOG.debug("max value: " + maxValue);

        // may be several categories with max count
        List<Category> categories = new ArrayList<>();

        for(Category key : counts.keySet()) {
            Long count = counts.get(key);
            if (count == maxValue) categories.add(key);
        }

        LOG.debug("returning list of categories: " + categories);
        return categories;
    }

    @Override
    public String spellQuery(String query) {
        List<Spellcheck> spell = spellcheckCommunication.spell(query);
        StringBuilder spelledQuery = new StringBuilder();

        for (String word : query.split(" ")) {
            boolean foundSuggestion = false;
            for (Spellcheck spellcheck : spell) {
                if (spellcheck.getWord().equals(word)) {
                    spelledQuery.append(" ").append(spellcheck.getSuggestions().get(0));
                    foundSuggestion = true;
                    break;
                }
            }
            if (!foundSuggestion) spelledQuery.append(" ").append(word);
        }
        String result = spelledQuery.toString().trim();
        LOG.debug("result of spell: from '%s' to '%s'".formatted(query,result));
        return result;
    }

    @Override
    public List<String> generateTags(String query) {
        List<String> tags = new ArrayList<>();

        for (String word : query.split(" ")) {
            String langOriginal = "ru-ru";
            String langTranslate = "ru-en";

            //detect language of word
            int firstWordCharacter = word.toLowerCase().charAt(0);
            if (!(firstWordCharacter >= 1072 && firstWordCharacter <= 1103)) {
                langOriginal = "en-en";
                langTranslate = "en-ru";
            }

            List<com.market.Communication.Entity.YandexDictionary.Dictionary> dictionaries = dictionaryCommunication.getDictionary(word,langOriginal).getDef();
            dictionaries.addAll(dictionaryCommunication.getDictionary(word,langTranslate).getDef());

            for (Dictionary dictionary : dictionaries) {
                for (Translation translation : dictionary.getTr()) {
                    tags.add(translation.getText());

                    if (translation.getSyn() != null) {
                        for (Synonym synonym : translation.getSyn()) {
                            tags.add(synonym.getText());
                        }
                    }
                }
            }
        }

        LOG.debug("generated list of tags by query '" + query + "': " + tags);
        return tags;
    }

    @Override
    public int compareEquality(Product product1, Product product2, String query) {
        query = query.toLowerCase();

        int p1score = 0;
        int p2score = 0;

        String p1info = (product1.getName() + product1.getDescription() + product1.getSeller().getTitle() + product1.getSeller().getDescription()).toLowerCase();
        String p2info = (product2.getName() + product2.getDescription() + product2.getSeller().getTitle() + product2.getSeller().getDescription()).toLowerCase();

        for (String word : query.split(" ")) {
            int count = 0;
            int index = 0;

            //p1
            while ((index = p1info.indexOf(word, index)) != -1) {
                count++;
                index += word.length();
            }
            p1score += count;

            count = 0;
            index = 0;
            while ((index = p2info.indexOf(word, index)) != -1) {
                count++;
                index += word.length();
            }
            p2score += count;
        }

        LOG.debug("calculated equality score for products. p1: " + product1 + ", p2: " + product2);
        LOG.debug("result is {p1 : " + p1score + ", p2 : " + p2score + "}");
        return p1score-p2score;
    }
}
