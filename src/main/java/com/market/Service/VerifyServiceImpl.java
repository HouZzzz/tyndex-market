package com.market.Service;

import com.market.DAO.Interfaces.CustomerDAO;
import com.market.DAO.Interfaces.SellerDAO;
import com.market.Entity.Customer;
import com.market.Entity.Seller;
import com.market.Service.Interfaces.MailNotificationService;
import com.market.Service.Interfaces.VerifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class VerifyServiceImpl implements VerifyService {
    @Autowired
    private MailNotificationService mailNotificationService;

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private SellerDAO sellerDAO;



    //   K:token | V: code
    private Map<String,String> codeByToken = new HashMap<>();

    //   K: code | V: user (customer/seller)
    private Map<String,Object> userByCode = new HashMap<>();

    private Logger LOG = LoggerFactory.getLogger(VerifyService.class);

    @Override
    public String generateVerifyCode(int length) {
        Random random = new Random();

        String numbers = "0123456789";
        StringBuilder code = new StringBuilder();

        while (code.length() < length) {
            code.append(numbers.charAt(random.nextInt(numbers.length())));
        }
        LOG.debug("generated verify code: " + code);

        return code.toString();
    }

    /**
     * @return code with 4 nums as default
     */
    @Override
    public String generateVerifyCode() {
        return generateVerifyCode(4);
    }

    @Override
    public boolean verify(String token, String email, String title, String name, String code) {
        LOG.debug("trying to verify user " + email + " by token " + token);

        String trueCode = codeByToken.get(token);
        Object trueUser = userByCode.get(code); // customer or seller

        codeByToken.remove(token);
        userByCode.remove(trueCode);

        if (trueCode == null || trueCode.isBlank()) {
            LOG.debug("verify rejected: cant find code by token " + token);
            return false;
        }

        if (!trueCode.equals(code)) {
            LOG.debug("verify rejected: enter code is not equal to generated code");
            return false;
        }

        String trueEmail = "";
        if (trueUser instanceof Customer c) trueEmail = c.getEmail();
        else if (trueUser instanceof Seller s) trueEmail = s.getEmail();
        else return false;

        if (trueEmail.equals(email)) {
            LOG.debug("verify completed");
            return true;
        }

        LOG.debug("verify rejected: user data is not equal to required params");
        return false;
    }

    @Override
    @Transactional
    public void startVerify(String title, String email, String name, String token) {
        Customer customer = new Customer();
        Seller seller = new Seller();

        boolean verifySeller = (title != null && !title.isEmpty()) && (name == null || name.isEmpty());
        if (verifySeller) {
            seller = sellerDAO.getSellerByTitle(title);
            if (seller == null) {
                seller = new Seller();
                seller.setTitle(title);
                seller.setEmail(email);
            }
        } else {
            customer = customerDAO.getCustomerByEmail(email);
            if (customer == null) {
                customer = new Customer();
                customer.setName(name);
                customer.setEmail(email);
            }
        }

        String code = generateVerifyCode();
        codeByToken.put(token,code);

        userByCode.put(code,verifySeller ? seller : customer);

        mailNotificationService.sendMail(
                email,
                "Подтверждение регистрации",
                "Ваш код подтверждения: " + code);

        LOG.debug("verify started for user " + email);
    }
}
