package com.market.Service;

import com.market.DAO.Interfaces.CustomerDAO;
import com.market.Entity.Customer;
import com.market.Service.Interfaces.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerDAO customerDAO;

    private Logger LOG = LoggerFactory.getLogger(CustomerService.class);

    @Override
    @Transactional
    public void registerCustomer(Customer customer) {
        // register only if email is unique
        if (customerDAO.getCustomerByEmail(customer.getEmail()) == null) {
            customerDAO.saveCustomer(customer);
        } else {
            LOG.debug("failed to register new customer: customer with email " + customer.getEmail() + " already exists");
        }
    }

    @Override
    @Transactional
    public Customer getCustomerByEmail(String email) {
        return customerDAO.getCustomerByEmail(email);
    }

    @Autowired
    public CustomerServiceImpl(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }
}
