package com.market.Service;

import com.market.Annotation.SellerRequired;
import com.market.DAO.Interfaces.CustomerDAO;
import com.market.DAO.Interfaces.SellerDAO;
import com.market.DAO.Interfaces.SessionDAO;
import com.market.Entity.Session;
import com.market.Service.Interfaces.UserCookieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import java.util.UUID;

@Service
public class UserCookieServiceImpl implements UserCookieService {
    @Autowired
    private SessionDAO sessionDAO;

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private SellerDAO sellerDAO;

    @Value("${cookie.maxage}")
    private String cookieMaxAge = "604800"; // week by default

    private Logger LOG = LoggerFactory.getLogger(UserCookieService.class);


    @Override
    @Transactional
    public Cookie generateSession(String email,String title) {
        boolean customer = email != null && title == null;
        String uuid = UUID.randomUUID().toString();

        Cookie cookie = new Cookie("tyndex", uuid);
        cookie.setMaxAge(Integer.parseInt(cookieMaxAge));
        cookie.setPath("/");

        Session session = new Session();
        session.setUuid(uuid);

        if (customer) {
            session.setCustomer(
                    customerDAO.getCustomerByEmail(email));
        } else { // seller
            session.setSeller(
                    sellerDAO.getSellerByTitle(title));
        }

        sessionDAO.saveSession(session);
        return cookie;
    }

    @Override
    @Transactional
    public Object getUserBySessionUUID(String uuid) {
        Session session = sessionDAO.getSessionByUUID(uuid);

        return session != null ?
            (session.getSeller() != null ? session.getSeller() : session.getCustomer())
        : null;
    }

    @Override
    public Cookie generateVerifyToken(String email) {
        String uuid = UUID.randomUUID().toString();
        Cookie cookie = new Cookie("tyndex-verify",uuid);
        cookie.setPath("/");
        cookie.setMaxAge(600); // 10 min

        return cookie;
    }

    @Override
    public Cookie killCookie(String cookieName) {
        Cookie cookie = new Cookie(cookieName,null);
        cookie.setMaxAge(0);
        return cookie;
    }
}
