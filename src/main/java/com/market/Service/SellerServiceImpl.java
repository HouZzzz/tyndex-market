package com.market.Service;

import com.market.DAO.Interfaces.ProductDAO;
import com.market.DAO.Interfaces.SellerDAO;
import com.market.Entity.Feedback;
import com.market.Entity.Seller;
import com.market.Service.Interfaces.SellerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SellerServiceImpl implements SellerService {
    private SellerDAO sellerDAO;
    private ProductDAO productDAO;

    private Logger LOG = LoggerFactory.getLogger(SellerService.class);

    @Override
    @Transactional
    public void registerSeller(Seller seller) {
        // register only if email is unique
        if (sellerDAO.getSellerByTitle(seller.getTitle()) == null) {
            sellerDAO.saveSeller(seller);
        } else {
            LOG.debug("failed to register new customer: customer with title " + seller.getTitle() + " already exists");
        }
    }

    @Override
    @Transactional
    public Seller getSellerByTitle(String title) {
        return sellerDAO.getSellerByTitle(title);
    }

    @Override
    @Transactional
    public Seller getSellerByID(int id) {
        return sellerDAO.getSellerByID(id);
    }

    @Override
    public String calculateAverageValue(int sellerID) {
        Seller seller = sellerDAO.getSellerByID(sellerID);
        List<Feedback> feedbacks = productDAO.getFeedbacksBySeller(seller);
        String average = String.format("%.1f",
                feedbacks
                        .stream()
                        .mapToInt(Feedback::getValue)
                        .average()
                        .orElse(0));
        LOG.debug("calculated average value: " + average);
        return average;
    }

    @Autowired
    public SellerServiceImpl(SellerDAO sellerDAO, ProductDAO productDAO) {
        this.sellerDAO = sellerDAO;
        this.productDAO = productDAO;
    }
}
