package com.market.Service;

import com.market.Service.Interfaces.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.HashMap;
import java.util.Map;

@Service
public class ValidationServiceImpl implements ValidationService {
    private Logger LOG = LoggerFactory.getLogger(ValidationService.class);

    @Override
    public void addErrorsToModel(Model model, BindingResult bindingResult) {
        Map<String, String> errors = new HashMap<>();

        bindingResult
                .getFieldErrors()
                .forEach(e -> errors.put(e.getField() + "Err",e.getDefaultMessage()));
        LOG.debug("generated map of errors by binding result: " + errors);

        for (String key : errors.keySet()) {
            model.addAttribute(key,errors.get(key));
        }
    }
}
