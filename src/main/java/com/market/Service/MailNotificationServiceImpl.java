package com.market.Service;

import com.market.Service.Interfaces.MailNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class MailNotificationServiceImpl implements MailNotificationService {
    @Value("${mail.username}")
    private String USER_NAME = "";

    @Value("${mail.password}")
    private String PASSWORD = "";

    private static Logger LOG = LoggerFactory.getLogger("Mail Notificator");

    public void sendMail(String to,String subject,String body) {
        new Thread(() -> {
            LOG.debug("sending mail to " + to);

            Properties props = System.getProperties();
            String host = "smtp.gmail.com";
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.user", USER_NAME);
            props.put("mail.smtp.password", PASSWORD);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);
            MimeMessage message = new MimeMessage(session);

            try {
                message.setFrom(new InternetAddress(USER_NAME));
                InternetAddress toAddress = new InternetAddress(to);

                message.addRecipient(Message.RecipientType.TO, toAddress);
                message.setSubject(subject);
                message.setText(body);
                Transport transport = session.getTransport("smtp");
                transport.connect(host, USER_NAME, PASSWORD);
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();

                LOG.debug("mail sent");
            } catch (AddressException ae) {
                ae.printStackTrace();
            } catch (MessagingException me) {
                me.printStackTrace();
            }
        }).start();
    }
}
