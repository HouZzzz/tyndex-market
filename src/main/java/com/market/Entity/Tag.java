package com.market.Entity;

import javax.persistence.*;

@Entity
@Table(name = "tag")
public class Tag {
    @Id
    @Column
    private String tag;

    @JoinColumn(name = "product_id")
    @ManyToOne
    private Product product;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tag='" + tag + '\'' +
                ", product=" + product +
                '}';
    }

    public Tag() {
    }

    public Tag(String tag, Product product) {
        this.tag = tag;
        this.product = product;
    }
}
