package com.market.Entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    @NotBlank(message = "Поле не должно быть пустым")
    @Size(min = 2, message = "Название должно содержать минимум 2 символа")
    private String name;

    @Column
    @NotBlank(message = "Поле не должно быть пустым")
    @Size(min = 10, message = "Описание должно содержать минимум 10 символов")
    private String description;

    @Column
    @Min(value = 100,message = "Цена товара должна быть не менее 100 рублей")
    private double price;

    @Column(name = "availability")
    private boolean available;


    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private Inventory inventory;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    private Seller seller;

    @Transient
    private String tags_list;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Tag> tags;


    // web wrapper fields
    @Transient
    @Min(value = 0,message = "Значение не должно быть меньше нуля")
    private int stockLevel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public int getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(int stockLevel) {
        this.stockLevel = stockLevel;
    }

    public String getTags_list() {
        return tags_list;
    }

    public void setTags_list(String tags_list) {
        this.tags_list = tags_list;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", available=" + available +
                ", category=" + category.getName() +
                ", seller=" + seller.getTitle() +
                ", tags_list='" + tags_list + '\'' +
                ", stockLevel=" + stockLevel +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id && Double.compare(product.price, price) == 0 && available == product.available && stockLevel == product.stockLevel && Objects.equals(name, product.name) && Objects.equals(description, product.description) && Objects.equals(category, product.category) && Objects.equals(inventory, product.inventory) && Objects.equals(seller, product.seller);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, available, category, inventory, seller, stockLevel);
    }
}
