package com.market.Entity;

import javax.persistence.*;

@Entity
@Table(name = "session")
public class Session {
    @Id
    @Column
    private String uuid;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    private Seller seller;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    @Override
    public String toString() {
        return "Session{" +
                "uuid='" + uuid + '\'' +
                ", customer=" + customer +
                ", seller=" + seller +
                '}';
    }
}
