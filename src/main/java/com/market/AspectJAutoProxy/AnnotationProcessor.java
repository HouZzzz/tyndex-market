package com.market.AspectJAutoProxy;

import com.market.Entity.Customer;
import com.market.Entity.Seller;
import com.market.Service.Interfaces.UserCookieService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class AnnotationProcessor {
    @Autowired
    private UserCookieService userCookieService;

    private Logger LOG = LoggerFactory.getLogger(AnnotationProcessor.class);

    @Around("@annotation(com.market.Annotation.SellerRequired)")
    public Object aroundSellerRequired(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        String principal = (String) args[args.length - 1];
        LOG.debug("handling @SellerRequired with sessionUUID " + principal);

        // if user is not seller or not authenticated
        if (principal == null || principal.isEmpty()) {
            LOG.debug("rejected: user is not seller");
            return "redirect:/";
        } else {
            Object user = userCookieService.getUserBySessionUUID(principal); // Customer or Seller

            if (!(user instanceof Seller)) {
                LOG.debug("rejected: user with session " + principal + " is not seller");
                return "redirect:/";
            }

            args[args.length - 2] = user;
            LOG.debug("set seller to args " + user);

            LOG.debug("check for seller permission passed successfully");
            return joinPoint.proceed(args);
        }
    }

    @Around("@annotation(com.market.Annotation.CustomerOptional)")
    public Object aroundCustomerOptional(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        String principal = (String) args[args.length - 1];
        LOG.debug("used method with @CustomerOptional with principal " + principal);

        Object user = userCookieService.getUserBySessionUUID(principal);
        if (user instanceof Customer customer) {
            args[args.length - 2] = customer;
        }
        return joinPoint.proceed(args);
    }
}
